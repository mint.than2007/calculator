from tkinter import *

# initial Tk library
root = Tk()
# define application title
root.title("Add numbers ")
# define application size
root.geometry("500x500")
# define header title

def addNum():
    a = n1.get()
    b = n2.get()
    c = a+b
    etSum.insert(0, c)


header = Label(root, text="Calculator", font=("Times New Roman", 30, "bold"))
header.pack(pady=5)

n1 = IntVar()
n2 = IntVar()

l1 = Label(root, text="First number ")
l1.pack()

et1 = Entry(root, textvariable=n1)
et1.pack()

l2 = Label(root, text="Second number ")
l2.pack()

et2 = Entry(root, textvariable=n2)
et2.pack()

btnAdd = Button(root, text="Calculate", command=addNum)
btnAdd.pack()
etSum=Entry(root)
etSum.pack()
root.mainloop() 